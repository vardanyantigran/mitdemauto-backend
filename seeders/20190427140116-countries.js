'use strict';
const countries = require('./country.json');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('countries', countries.map(item => ({
      name: item.name,
      alpha2: item['alpha-2'],
      alpha3: item['alpha-3'],
      region: item.region,
      sub_region: item['sub-region'],
      dial_code: item.dial_code,
    })), {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('countries', null, {});
  }
};
