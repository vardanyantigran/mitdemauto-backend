'use strict';
const phones = require('./phone.json');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('phones', phones, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('phones', null, {});
  }
};
