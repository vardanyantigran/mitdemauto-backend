'use strict';
const ordersStatus = require('./ordersStatus.json');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('order_status', ordersStatus, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('order_status', null, {});
  }
};
