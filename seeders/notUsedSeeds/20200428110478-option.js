'use strict';
const option = [
  {
    "key": "guest_user_role_id",
    "value": "3"
  }
];

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('options', option, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('options', null, {});
  }
};
