import path from 'path';
import request from 'request';
import nodemailer from 'nodemailer';
import express from 'express';
import bodyParser from 'body-parser';
import fileUpload from 'express-fileupload';
import expressValidator from 'express-validator';
import logger from 'morgan';
import Sequelize from 'sequelize';
import redis from 'redis';
import cron from 'node-cron';
import { exec } from 'child_process';
import * as admin from 'firebase-admin';
import socketS from 'socket.io';
import { setUpDb } from './api/models';
import enableRoutes from './api';
import sequelizeConfig from '../sequelizeConfig';
import { ErrorHendler } from './api/modules/errorHandler';

class Application {
  constructor() {
    this.app = express();
    this.initApp();
  }

  initApp() {
    this.configApp();
    this.dbConfig();
    this.dbRedisConfig();
    this.emailConfig();
    this.setCronJobs();
    this.setParams();
    this.setRouter();
    this.setErrorHandler();
    this.set404Handler();
  }

  configApp() {
    this.app.use(bodyParser.json({
      limit: '5mb',
    }));
    this.app.use(bodyParser.urlencoded({
      limit: '5mb',
      extended: true,
    }));
    this.app.use(fileUpload({
      limits: { fileSize: 5 * 1024 * 1024 },
    }));
    this.app.use(expressValidator());
    this.app.use((req, res, next) => {
      res.setHeader('Access-Control-Allow-Origin', '*');
      res.setHeader('Access-Control-Allow-Credentials', true);
      res.setHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
      res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
      if (req.method === 'OPTIONS') {
        return res.json();
      }
      return next();
    });
    this.app.use((req, res, next) => {
      res.responseHandler = function responseHandler(data = null, message = '', status = 200, name = 'SUCCESS') {
        this.status(status).json({
          status: name,
          message,
          data,
          errors: null,
        });
      };
      return next();
    });
    this.app.use(logger('combined'));
    if (process.NODE_ENV !== 'production') {
      this.app.use('/api/assets', express.static(path.join(__dirname, '../', 'public')));
    }
  }

  // eslint-disable-next-line class-methods-use-this
  dbConfig() {
    const sequelize = new Sequelize(sequelizeConfig);
    const models = setUpDb(sequelize);

    const io = socketS.listen(5555);

    io.sockets.on('connection', async (socket) => {
      const orderReview = await models.orderReview.findAll();
      const rooms = orderReview.map(r => r.id);
      socket.emit('setup', { rooms });

      socket.on('new user', async (data) => {
        socket.join(data.room);
        const orderReviewCustom = await models.orderReview.findByPk(data);
        const conversation = orderReviewCustom.conversation || [];
        io.in(data.room).emit('message', { reviewId: data, conversation });
      });

      socket.on('new message', async (data) => {
        socket.join(data.room);
        const orderReviewCustom = await models.orderReview.findByPk(data.room);
        const conversation = orderReviewCustom.conversation || [];
        conversation.push({ by: data.by, text: data.text });
        models.orderReview.update({ conversation }, { where: { id: data.room } });
        io.in(data.room).emit('message', { reviewId: data.room, conversation });
      });
    });
  }

  // eslint-disable-next-line class-methods-use-this
  dbRedisConfig() {
    const client = redis.createClient({
      port: process.env.REDIS_PORT,
      host: process.env.REDIS_HOST,
    });
    client.on('error', (error) => {
      console.log(`Error REDIS ${error}`);
    });
    client.on('connect', () => {
      console.log('REDIS connect ready');
    });
    global.redisClient = client;
  }

  // eslint-disable-next-line class-methods-use-this
  firebaseConfig() {
    // eslint-disable-next-line global-require
    const firebaseConfig = require('../firebasePrivateConfig.json');
    admin.initializeApp({
      credential: admin.credential.cert(firebaseConfig),
      databaseURL: process.env.FIREBASE_DB_URL,
    });
  }

  emailConfig() {
    global.transportMenu4u = nodemailer.createTransport({
      host: process.env.MAIL_HOST,
      port: process.env.MAIL_PORT,
      secure: process.env.MAIL_SSL,
      auth: {
        user: process.env.MAIL_USERNAME,
        pass: process.env.MAIL_PASSWORD,
      },
    });
    this.transportmenu4u = global.transportmenu4u;
  }

  // eslint-disable-next-line class-methods-use-this
  async sendSlackError(message, ch) { // TO DO
    return new Promise((resolve, reject) => {
      const options = {
        method: 'POST',
        url: `${process.env.SLACK_URL}/services/TJ46Q7DFZ/BJEC89MGQ/${ch}`,
        headers:
        {
          'Content-Type': 'application/json',
        },
        body: { text: message },
        json: true,
      };
      request(options, (error, response, body) => {
        if (error) reject(error);
        resolve(body);
      });
    });
  }

  // eslint-disable-next-line class-methods-use-this
  setCronJobs() {
    // cron.schedule('* * * * *', () => {
    //   console.log('============ CRON START =============');
    //   exec('node_modules/.bin/sequelize db:seed --seed clearEmails.js --seeders-path ./cronJobs');
    //   console.log('============= CRON END ==============');
    // });
  }

  setParams() {
    this.app.set('env', process.env.NODE_ENV);
  }

  setRouter() {
    this.router = express.Router();
    this.app.use('/api', enableRoutes(this.router));
  }

  set404Handler() {
    this.app.use((req, res) => {
      res.status(404).json({
        status: 'Error',
        message: '',
        data: null,
        errors: '',
      });
    });
  }

  setErrorHandler() {
    // eslint-disable-next-line no-unused-vars
    this.app.use((error, req, res, next) => {
      // console.error(error);
      if (error.formatWith && typeof error.formatWith === 'function') {
        return res.status(403).json({
          status: 'Error',
          message: error.message,
          data: null,
          errors: error.mapped(),
        });
      }
      console.log(error, error instanceof ErrorHendler);
      if (error instanceof ErrorHendler) {
        return res.status(error.status || 400).json({
          status: error.name,
          message: error.message,
          data: null,
          errors: error.errors,
        });
      }
      this.sendSlackError(`\`\`\`${error.stack}\`\`\``, process.env.SLACK_CH_ERROR).then();
      return res.status(500).json({
        status: 'Error',
        message: error.message,
        data: null,
        errors: null,
      });
    });
  }
}
const instnseOfApplication = new Application();
export default () => instnseOfApplication.app;
