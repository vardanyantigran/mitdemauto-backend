import * as express from 'express';
import Routes from './carRouter';

class CarModule {
  constructor(apiRouter) {
    this.router = express.Router();
    this.apiRouter = apiRouter;
  }

  createEndpoints() {
    this.assignRouter();
    this.assignEndpoints();
  }

  assignRouter() {
    this.apiRouter.use('/car', this.router);
  }

  assignEndpoints() {
    Routes(this.router);
  }
}
export default apiRouter => new CarModule(apiRouter);
