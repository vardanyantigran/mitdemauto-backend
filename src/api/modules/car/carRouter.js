import nodemailer from 'nodemailer';
import {
  show,
  store,
  showAll,
  update,
  remove,
  bulkUpdate,
  bulkRemove,
} from './carController';
import { checkAuthentication, checkPermissions } from '../../utils/auth';
import { CAR_EDIT } from '../user/permissions';
import {
  carValidator,
  storeCarValidator,
  updateCarValidator,
} from '../../utils/requestValidations';

export default (router) => {
  const transporter = nodemailer.createTransport({
    host: 'smtp.dreamhost.com',
    port: 465,
    secure: true, // upgrade later with STARTTLS
    auth: {
      user: 'support@editmentor.com',
      pass: 'v5VRFZJ?',
    },
  });
  const message = {
    from: 'support@editmentor.com',
    to: 'tigran@brainbox.am',
    subject: 'Message title',
    text: 'Plaintext version of the message',
    html: '<p>HTML version of the message</p>',
    dsn: {
      id: 'some random message specific id',
      return: 'headers',
      notify: ['failure', 'delay', 'success'],
      recipient: 'tavardanyan@yandex.com',
    },
  };
  router.get('/b', async (req, res, next) => {
    const resp = await transporter.sendMail(message);
    return res.responseHandler(resp);
  });
  router.use(checkAuthentication);
  // =======================================================================================
  router.use(checkPermissions(CAR_EDIT));
  // =======================================================================================
  router.get('/:id', carValidator, show);
  // ---------------------------------------------------------------------------------------
  router.get('/', showAll);
  // ---------------------------------------------------------------------------------------
  router.post('/', storeCarValidator, store);
  // ---------------------------------------------------------------------------------------
  router.put('/:id', updateCarValidator, update);
  // ---------------------------------------------------------------------------------------
  router.put('/update/', bulkUpdate);
  // ---------------------------------------------------------------------------------------
  router.delete('/:id', carValidator, remove);
  // ---------------------------------------------------------------------------------------
  router.delete('/remove/', bulkRemove);
  // ---------------------------------------------------------------------------------------
};
