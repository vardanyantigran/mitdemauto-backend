import models from '../../models';
import updatedData from '../../utils/sanitizer';

export async function showAll(req, res, next) {
  try {
    const cars = await models.car.findAll({
      where: { userId: req.user.id },
      include: ['user'],
    });
    return res.responseHandler(cars);
  } catch (error) {
    return next(error);
  }
}

export async function show(req, res, next) {
  try {
    const car = await models.car.findOne({
      where: { id: req.params.id },
      include: ['user'],
    });
    return res.responseHandler(car);
  } catch (error) {
    return next(error);
  }
}

export async function store(req, res, next) {
  try {
    const car = await models.car.create(req.body);
    car.addImages(req.body.images);
    return res.responseHandler(car);
  } catch (error) {
    return next(error);
  }
}

export async function update(req, res, next) {
  try {
    const car = await models.car.update(req.body, {
      where: { id: req.params.id },
      returning: true,
    });
    return res.responseHandler(updatedData(car));
  } catch (error) {
    return next(error);
  }
}

export function bulkUpdate(req, res, next) {
  try {
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function remove(req, res, next) {
  try {
    const car = await models.car.destroy({
      where: { id: req.params.id },
    });
    return res.responseHandler(car);
  } catch (error) {
    return next(error);
  }
}

export function bulkRemove(req, res, next) {
  try {
    return next();
  } catch (error) {
    return next(error);
  }
}
