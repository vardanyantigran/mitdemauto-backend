import models from '../../models';
import updatedData from '../../utils/sanitizer';

export async function createRole(req, res, next) {
  try {
    const role = await models.role.create(req.body);
    return res.responseHandler(role);
  } catch (error) {
    return next(error);
  }
}

export async function getRoles(req, res, next) {
  try {
    const roles = await models.role.findAll();
    return res.responseHandler(roles);
  } catch (error) {
    return next(error);
  }
}

export async function updateRole(req, res, next) {
  try {
    const role = await models.role.update(req.body, {
      where: {
        id: req.params.id,
      },
      returning: true,
    });
    return res.responseHandler(updatedData(role));
  } catch (error) {
    return next(error);
  }
}

export async function removeRole(req, res, next) {
  try {
    const role = await models.role.destroy({
      where: {
        id: req.params.id,
      },
    });
    return res.responseHandler(role);
  } catch (error) {
    return next(error);
  }
}

export async function getRoleValue(req, res, next) { // TO DO
  try {
    const role = await models.role.findOne({
      where: { id: req.params.id },
      attributes: ['name', 'value'],
    });
    return res.responseHandler(role);
  } catch (error) {
    return next(error);
  }
}

export async function test(req, res, next) {
  try {
    // const r = await models.product.create({
    //   sku: 'KKF523MsN123',
    //   barcode: '48600s759414',
    //   name: 'New Product Name',
    //   isBlocked: false,
    // eslint-disable-next-line max-len
    //   description: 'Non nulla esse qui reprehenderit culpa nulla cupidatat ut. Proident consequat labore est ullamco laborum ipsum reprehenderit. Eu amet quis ex aliquip magna elit. Do eiusmod cupidatat ut nostrud do velit enim excepteur adipisicing dolore duis.',
    //   keywords: 'New',
    //   price: 170,
    //   minPrice: 125.5,
    //   unit: 1,
    //   step: 1,
    //   createdByUserId: 3,
    //   updatedByUserId: 3,
    //   tags: [
    //     { name: 'Gammammm', createdByUserId: 3, updatedByUserId: 3 },
    //   ],
    // }, {
    //   include: [models.tag],
    // });
    const user = await models.user.findOne();
    console.log(models.user.prototype);
    return res.responseHandler(await user.countSales());
  } catch (error) {
    return next(error);
  }
}
