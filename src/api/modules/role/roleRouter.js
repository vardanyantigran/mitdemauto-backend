import {
  getRoles,
  getRoleValue,
  createRole,
  updateRole,
  removeRole,
} from './roleController';
import { checkAuthentication, checkPermissions } from '../../utils/auth';
import { USER_EDIT } from '../user/permissions';
import {
  roleValidator,
  roleUpdateValidator,
} from '../../utils/requestValidations';

export default (router) => {
  // =======================================================================================
  router.use(checkAuthentication);
  router.use(checkPermissions(USER_EDIT));
  // =======================================================================================
  router.get('/', getRoles);
  // ---------------------------------------------------------------------------------------
  router.get('/:id', getRoleValue); // TO DO
  // ---------------------------------------------------------------------------------------
  router.post('/', roleValidator, createRole);
  // ---------------------------------------------------------------------------------------
  router.put('/:id', roleUpdateValidator, updateRole);
  // ---------------------------------------------------------------------------------------
  router.delete('/:id', removeRole);
};
