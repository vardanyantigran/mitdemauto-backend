import models from '../../models';

export async function showAll(req, res, next) {
  try {
    const countries = await models.country.findAll();
    return res.responseHandler(countries);
  } catch (error) {
    return next(error);
  }
}

export async function show(req, res, next) {
  try {
    const country = await models.country.findOne({
      where: { id: req.params.id },
    });
    return res.responseHandler(country);
  } catch (error) {
    return next(error);
  }
}
