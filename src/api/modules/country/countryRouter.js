import {
  show,
  showAll,
} from './countryController';
import {
  countryValidator,
} from '../../utils/requestValidations';

export default (router) => {
  router.get('/', showAll);
  // ---------------------------------------------------------------------------------------
  router.get('/:id', countryValidator, show);
};
