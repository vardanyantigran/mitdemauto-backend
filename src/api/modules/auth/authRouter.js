import { checkAuthentication } from '../../utils/auth';
import {
  signUp,
  signIn,
  signOut,
  checkEmail,
  checkUsername,
  activateEmail,
  forgot,
} from './authController';
import {
  signUpValidator,
  signInValidator,
  checkEmailValidator,
  checkUsernameValidator,
  activateEmailValidator,
} from '../../utils/requestValidations';

export default (router) => {
  router.post('/signup', signUpValidator, signUp);
  // ----------------------------------------------------------------------------------------------
  router.post('/signin', signInValidator, signIn);
  // ----------------------------------------------------------------------------------------------
  router.post('/forgot', forgot);
  // ----------------------------------------------------------------------------------------------
  router.get('/signout', checkAuthentication, signOut);
  // ----------------------------------------------------------------------------------------------
  router.get('/email/:email', checkEmailValidator, checkEmail);
  // ----------------------------------------------------------------------------------------------
  router.get('/username/:username', checkUsernameValidator, checkUsername);
  // ----------------------------------------------------------------------------------------------
  router.get('/approveemail/:activateCode', activateEmailValidator, activateEmail);
  // ----------------------------------------------------------------------------------------------
};
