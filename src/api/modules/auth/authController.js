import crypto from 'crypto';
import uuid from 'uuid/v4';
import { Op } from 'sequelize';
import jwt from 'jsonwebtoken';
import { validationResult } from 'express-validator/check';
import models from '../../models';
import { AuthError, BadRequest } from '../errorHandler';
import { redisDel } from '../../utils/auth';
import { sendSignUpEmail, decrypt } from '../../utils/mailing';

export async function signUp(req, res, next) {
  const code = uuid();
  const transaction = await models.sequelize.transaction();
  try {
    req.body.email = req.body.username;
    const { id } = await models.user.create({
      ...req.body,
      password: crypto.createHash('sha1').update(req.body.password).digest('hex'),
      roleId: 1, // TO DO
    }, { transaction });
    const email = await models.email.create({
      ...req.body,
      code,
      userId: id,
    }, { transaction });
    if (req.body.phone) {
      await models.phone.create({
        number: req.body.phone,
        countryId: req.body.phoneCountryId,
        userId: id,
      }, { transaction });
    }
    if (req.body.city) {
      await models.address.create({
        ...req.body,
        countryId: req.body.addressCountryId,
        userId: id,
      }, { transaction });
    }
    await transaction.commit();
    const user = await models.user.findByPk(id, { include: [{ all: true }] });
    sendSignUpEmail(code, email, user.firstName, user.lastName).then();
    return res.responseHandler(user);
  } catch (error) {
    if (transaction.finished !== 'commit') {
      await transaction.rollback();
    }
    return next(error);
  }
}

export async function signIn(req, res, next) {
  try {
    validationResult(req).throw();
    const { username = '' } = req.body;
    let userWrapper = await models.user.findOne({
      attributes: { include: ['password'] },
      where: { username },
      include: [
        { model: models.email, required: false }, // ? required
        { model: models.phone, include: 'country' },
        { model: models.address, include: 'country' },
        { model: models.role },
        { model: models.car },
      ],
    });
    if (!userWrapper) {
      const email = await models.email.findOne({
        where: { email: req.body.username },
        include: [{ model: models.user }],
        returning: true,
      });
      const { user: { id: userId } } = email.get({ plain: true });
      userWrapper = await models.user.findByPk(userId, {
        attributes: { include: ['password'] },
        include: [
          { model: models.email, required: false }, // ? required
          { model: models.phone, include: 'country' },
          { model: models.address, include: 'country' },
          { model: models.role },
        ],
      });
      if (!userWrapper) {
        throw new AuthError('Wrong username or email');
      }
    }
    const { password, ...user } = userWrapper.get({ plain: true });
    if (password !== crypto.createHash('sha1').update(req.body.password).digest('hex')) {
      throw new AuthError('Wrong password');
    }
    const sessionId = uuid();
    global.redisClient.set(sessionId, JSON.stringify(user));
    return res.responseHandler({
      token: jwt.sign({ id: user.id, sessionId }, process.env.JWT_SECRET),
      user,
    });
  } catch (error) {
    return next(error);
  }
}

export async function signOut(req, res, next) {
  try {
    const { sessionId } = jwt.verify(req.headers.authorization, process.env.JWT_SECRET);
    await redisDel(sessionId);
    return res.responseHandler(null, 'success logout');
  } catch (error) {
    return next(error);
  }
}

export async function checkEmail(req, res, next) {
  try {
    validationResult(req).throw();
    return res.responseHandler(true);
  } catch (error) {
    return next(error);
  }
}

export async function checkUsername(req, res, next) {
  try {
    validationResult(req).throw();
    return res.responseHandler(true);
  } catch (error) {
    return next(error);
  }
}

export async function activateEmail(req, res, next) {
  try {
    const { activateCode } = req.params;
    const [code, id] = decrypt(activateCode).split('$');
    const transaction = await models.sequelize.transaction();
    let isEmailApproved = false;
    try {
      const [count, [email]] = await models.email.update({
        approved: true,
      }, {
        where: {
          id, code, approved: false,
        },
        returning: true,
        transaction,
      });
      switch (count) {
        case 0:
          throw new BadRequest('past 24 houre or you try second time');
        case 1:
          isEmailApproved = true;
          await models.user.update({
            defaultEmailId: email.id,
          }, {
            where: {
              id: email.userId,
            },
            transaction,
          });
          break;
        default:
          throw new BadRequest('undendler case ');
      }
      await transaction.commit();
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
    return res.responseHandler(isEmailApproved);
  } catch (error) {
    return next(error);
  }
}

export async function forgot(req, res, next) {
  try {
    const code = uuid();
    const user = await models.user.findOne({ where: { email: req.body.email }});
    sendForgotPasswordEmail(code, req.body.email, user.firstName, user.lastName);
    return res.responseHandler('ok');
  } catch (error) {
    return next(error);
  }
}
