import { store, show } from './imageController';
import { productImageUpload } from '../../utils/image';

export default (router) => {
  router.post('/', productImageUpload, store);
  router.get('/:url', show);
};
