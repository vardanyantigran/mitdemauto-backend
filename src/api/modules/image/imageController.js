import fs from 'fs';
import path from 'path';
import uuid from 'uuid/v4';
import models from '../../models';
import { sendSlackMessage } from '../../utils/mailing';
import { NotFound } from '../errorHandler';

export async function show(req, res, next) {
  try {
    const imagePath = path.join(__dirname, '../../../../uploads/image/', req.params.url);
    if (fs.existsSync(imagePath)) {
      return res.sendFile(imagePath);
    }
    throw new NotFound();
  } catch (error) {
    return next(error);
  }
}

export async function store(req, res, next) {
  try {
    if (Array.isArray(req.files.image)) {
      const imageArray = req.files.image.map(async (item) => {
        const imageName = `${uuid()}${path.extname(item.name)}`;
        const url = path.join(__dirname, '../../../../uploads/image/', imageName);
        if (!fs.existsSync(path.join(__dirname, '../../../../uploads/image/'))) {
          fs.mkdirSync(path.join(__dirname, '../../../../uploads/image/'));
        }
        fs.writeFile(url, item.data, (error) => {
          if (error) {
            sendSlackMessage(`image upload error IMAGE:${imageName}`);
          }
        });
        const img = await models.image.create({
          url: imageName,
        });
        const response = await img.get({
          plain: true,
          attributes: ['id', 'url'],
        });
        return response;
      });
      const responseImageData = [];
      await Promise.all(imageArray).then((values) => {
        responseImageData.push(values);
      });
      return res.responseHandler(responseImageData[0]);
    }
    const imageName = `${uuid()}${path.extname(req.files.image.name)}`;
    const url = path.join(__dirname, '../../../../uploads/image/', imageName);
    if (!fs.existsSync(path.join(__dirname, '../../../../uploads/image/'))) {
      fs.mkdirSync(path.join(__dirname, '../../../../uploads/image/'));
    }
    fs.writeFile(url, req.files.image.data, (error) => {
      if (error) {
        sendSlackMessage(`image upload error IMAGE:${imageName}`);
      }
    });
    const img = await models.image.create({
      url: imageName,
    });
    const response = await img.get({
      plain: true,
      attributes: ['id', 'url'],
    });
    return res.responseHandler(response);
  } catch (error) {
    return next(error);
  }
}

export async function update(req, res, next) {
  try {
    return next();
  } catch (error) {
    return next(error);
  }
}

export function bulkUpdate(req, res, next) {
  try {
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function remove(req, res, next) {
  try {
    return next();
  } catch (error) {
    return next(error);
  }
}

export function bulkRemove(req, res, next) {
  try {
    return next();
  } catch (error) {
    return next(error);
  }
}
