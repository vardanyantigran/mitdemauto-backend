import {
  show,
  showAll,
  store,
  update,
  remove,
  showTerms,
} from './optionController';
import { checkAuthentication } from '../../utils/auth';

export default (router) => {
  router.get('/', checkAuthentication, showAll);
  // ---------------------------------------------------------------------------------------
  router.get('/terms', showTerms);
  // ---------------------------------------------------------------------------------------
  router.get('/:key', show);
  // ---------------------------------------------------------------------------------------
  router.post('/', store);
  // ---------------------------------------------------------------------------------------
  router.put('/:id', update);
  // ---------------------------------------------------------------------------------------
  router.delete('/:id', remove);
};
