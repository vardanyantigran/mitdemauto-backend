import fs from 'fs';
import path from 'path';
import handlebars from 'handlebars';
import models from '../../models';

export async function showAll(req, res, next) {
  try {
    const options = await models.option.findAll();
    return res.responseHandler(options);
  } catch (error) {
    return next(error);
  }
}

export async function show(req, res, next) {
  try {
    const option = await models.option.findOne({
      where: { key: req.params.key },
    });
    return res.responseHandler(option);
  } catch (error) {
    return next(error);
  }
}

export async function store(req, res, next) {
  try {
    const option = await models.option.create(req.body);
    return res.responseHandler(option);
  } catch (error) {
    return next(error);
  }
}

export async function update(req, res, next) {
  try {
    const option = await models.option.update(req.body, {
      where: { id: req.params.id },
    });
    return res.responseHandler(option);
  } catch (error) {
    return next(error);
  }
}

export async function remove(req, res, next) {
  try {
    const option = await models.option.destroy({
      where: {
        id: req.params.id,
      },
    });
    return res.responseHandler(option);
  } catch (error) {
    return next(error);
  }
}

export async function showTerms(req, res, next) {
  try {
    const options = {};
    const html = await fs.readFileSync(path.join(__dirname, '../../utils/', 'terms', 'index.html'), { encoding: 'utf-8' });
    const template = handlebars.compile(html)(options);
    return res.send(template);
  } catch (error) {
    return next(error);
  }
}
