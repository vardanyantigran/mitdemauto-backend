import models from '../../models';
import updatedData from '../../utils/sanitizer';

export async function showAll(req, res, next) {
  try {
    const services = await models.service.findAll();
    return res.responseHandler(services);
  } catch (error) {
    return next(error);
  }
}

export async function show(req, res, next) {
  try {
    const service = await models.service.findOne({
      where: { id: req.params.id },
    });
    return res.responseHandler(service);
  } catch (error) {
    return next(error);
  }
}

export async function store(req, res, next) {
  try {
    const service = await models.service.create(req.body);
    if (req.body.address) {
      const address = await models.address.create(req.body.address);
      await service.setAddress(address);
    }
    return res.responseHandler(service);
  } catch (error) {
    return next(error);
  }
}

export async function update(req, res, next) {
  try {
    const service = await models.service.update(req.body, {
      where: {
        id: req.params.id,
      },
      returning: true,
    });
    await models.address.update(req.body.address, {
      where: {
        id: updatedData(service).addressId,
      },
    });
    return res.responseHandler(updatedData(service));
  } catch (error) {
    return next(error);
  }
}

export function bulkUpdate(req, res, next) {
  try {
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function remove(req, res, next) {
  try {
    const service = await models.service.destroy({
      where: {
        id: req.params.id,
      },
    });
    return res.responseHandler(service);
  } catch (error) {
    return next(error);
  }
}

export function bulkRemove(req, res, next) {
  try {
    return next();
  } catch (error) {
    return next(error);
  }
}
