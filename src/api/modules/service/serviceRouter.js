import {
  show,
  store,
  showAll,
  update,
  remove,
  bulkUpdate,
  bulkRemove,
} from './serviceController';
import { checkAuthentication, checkPermissions } from '../../utils/auth';
import { SERVICE_EDIT } from '../user/permissions';
import {
  serviceValidator,
  storeServiceValidator,
  updateServiceValidator,
} from '../../utils/requestValidations';

export default (router) => {
  router.get('/', showAll);
  // ---------------------------------------------------------------------------------------
  router.get('/:id', serviceValidator, show);
  // =======================================================================================
  router.use(checkAuthentication);
  router.use(checkPermissions(SERVICE_EDIT));
  // =======================================================================================
  router.post('/', storeServiceValidator, store);
  // ---------------------------------------------------------------------------------------
  router.put('/:id', updateServiceValidator, update);
  // ---------------------------------------------------------------------------------------
  router.put('/update/', bulkUpdate);
  // ---------------------------------------------------------------------------------------
  router.delete('/:id', serviceValidator, remove);
  // ---------------------------------------------------------------------------------------
  router.delete('/remove/', bulkRemove);
  // ---------------------------------------------------------------------------------------
};
