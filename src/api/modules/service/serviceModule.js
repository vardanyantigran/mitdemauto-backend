import * as express from 'express';
import Routes from './serviceRouter';

class ServiceModule {
  constructor(apiRouter) {
    this.router = express.Router();
    this.apiRouter = apiRouter;
  }

  createEndpoints() {
    this.assignRouter();
    this.assignEndpoints();
  }

  assignRouter() {
    this.apiRouter.use('/service', this.router);
  }

  assignEndpoints() {
    Routes(this.router);
  }
}
export default apiRouter => new ServiceModule(apiRouter);
