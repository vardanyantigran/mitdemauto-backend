import models from '../../models';
import updatedData from '../../utils/sanitizer';

export async function showAll(req, res, next) {
  try {
    const categories = await models.category.findAll({
      include: [
        {
          model: models.service,
        },
      ],
    });
    return res.responseHandler(categories);
  } catch (error) {
    return next(error);
  }
}

export async function show(req, res, next) {
  try {
    const category = await models.category.findOne({
      where: { id: req.params.id },
      include: [
        {
          model: models.service,
        },
      ],
    });
    return res.responseHandler(category);
  } catch (error) {
    return next(error);
  }
}

export async function store(req, res, next) {
  try {
    const category = await models.category.create({ ...req.body });
    return res.responseHandler(category);
  } catch (error) {
    return next(error);
  }
}

export async function update(req, res, next) {
  try {
    const category = await models.category.update({ ...req.body, ...req.updatedBy }, {
      where: {
        id: req.params.id,
      },
      returning: true,
    });
    return res.responseHandler(updatedData(category));
  } catch (error) {
    return next(error);
  }
}

export function bulkUpdate(req, res, next) {
  try {
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function remove(req, res, next) {
  try {
    const category = await models.category.destroy({
      where: {
        id: req.params.id,
      },
    });
    return res.responseHandler(category);
  } catch (error) {
    return next(error);
  }
}

export function bulkRemove(req, res, next) {
  try {
    return next();
  } catch (error) {
    return next(error);
  }
}
