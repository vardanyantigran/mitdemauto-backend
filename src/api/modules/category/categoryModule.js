import * as express from 'express';
import Routes from './categoryRouter';

class CategoryModule {
  constructor(apiRouter) {
    this.router = express.Router();
    this.apiRouter = apiRouter;
  }

  createEndpoints() {
    this.assignRouter();
    this.assignEndpoints();
  }

  assignRouter() {
    this.apiRouter.use('/category', this.router);
  }

  assignEndpoints() {
    Routes(this.router);
  }
}
export default apiRouter => new CategoryModule(apiRouter);
