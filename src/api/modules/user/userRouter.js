import {
  getCurrentUser,
  getUsers,
  createUser,
  test,
  getPermissions,
  updateUser,
  get,
  getCompanies,
  getSetting,
} from './userController';
import { checkAuthentication, checkPermissions } from '../../utils/auth';
import { USER_VIEW, PERMISSION, USER_EDIT } from './permissions';
import {
  userCreateValidator,
  userUpdateValidator,
} from '../../utils/requestValidations';

export default (router) => {
  router.get('/test/', test);
  // =======================================================================================
  router.use(checkAuthentication);
  // =======================================================================================
  router.get('/me', getCurrentUser);
  // ---------------------------------------------------------------------------------------
  router.get('/', checkPermissions(USER_VIEW), getUsers);
  // ---------------------------------------------------------------------------------------
  router.get('/permissions', checkPermissions(PERMISSION), getPermissions);
  // ---------------------------------------------------------------------------------------
  router.post('/company', getCompanies);
  // ---------------------------------------------------------------------------------------
  router.get('/setting', getSetting);
  // ---------------------------------------------------------------------------------------
  router.get('/:id', checkPermissions(USER_VIEW), get);
  // =======================================================================================
  router.use(checkPermissions(USER_EDIT));
  // =======================================================================================
  router.post('/', userCreateValidator, createUser);
  // ---------------------------------------------------------------------------------------
  router.put('/:id', userUpdateValidator, updateUser);
};
