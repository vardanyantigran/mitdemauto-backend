import * as express from 'express';
import Routes from './userRouter';

class UserModule {
  constructor(apiRouter) {
    this.router = express.Router();
    this.apiRouter = apiRouter;
  }

  createEndpoints() {
    this.assignRouter();
    this.assignEndpoints();
  }

  assignRouter() {
    this.apiRouter.use('/user', this.router);
  }

  assignEndpoints() {
    Routes(this.router);
  }
}
export default apiRouter => new UserModule(apiRouter);
