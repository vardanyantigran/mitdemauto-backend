import crypto from 'crypto';
import models from '../../models';
import * as permissions from './permissions';
import { sendNewUserPasswordEmail } from '../../utils/mailing';

export async function get(req, res, next) {
  try {
    const users = await models.user.findOne({
      include: [
        { model: models.phone, include: 'country' },
        { model: models.address, include: 'country' },
        { model: models.role },
        { model: models.car },
        { model: models.order, as: 'client' },
        { model: models.order, as: 'company' },
        { model: models.service },
      ],
      where: {
        id: req.params.id,
      },
    });
    return res.json(users);
  } catch (error) {
    return next(error);
  }
}

export async function getCompanies(req, res, next) {
  try {
    const users = await models.user.findAll({
      where: {
        roleId: 3,
      },
      include: [
        { model: models.email, required: false }, // ? required
        { model: models.phone, include: 'country' },
        { model: models.address, include: 'country' },
        { model: models.role },
        { model: models.car },
        { model: models.service },
        { model: models.image },
        { model: models.orderReview },
      ],
    });
    return res.responseHandler(users);
  } catch (error) {
    return next(error);
  }
}

export async function getCurrentUser(req, res, next) {
  try {
    const user = await models.user.findOne({
      include: [
        { model: models.phone, include: 'country' },
        { model: models.address, include: 'country' },
        { model: models.role },
        { model: models.car },
        { model: models.order, as: 'client' },
        { model: models.order, as: 'company' },
        { model: models.service },
      ],
      where: {
        id: req.user.id,
      },
    });
    return res.responseHandler(user);
  } catch (error) {
    return next(error);
  }
}

export async function getUsers(req, res, next) {
  try {
    const users = await models.user.findAll({
      include: [
        { model: models.email, required: false }, // ? required
        { model: models.phone, include: 'country' },
        { model: models.address, include: 'country' },
        { model: models.role },
        { model: models.car },
        { model: models.service },
      ],
    });
    return res.responseHandler(users);
  } catch (error) {
    return next(error);
  }
}

export async function createUser(req, res, next) {
  const password = Math.random().toString(36).slice(-8);
  const transaction = await models.sequelize.transaction();
  try {
    // req.body.username = req.body.userName ? req.body.userName : req.body.username;
    const { id } = await models.user.create({
      ...req.body,
      password: crypto.createHash('sha1').update(password).digest('hex'),
    }, { transaction });
    const email = await models.email.create({
      ...req.body,
      approved: true,
      userId: id,
    }, { transaction });
    if (req.body.phone) {
      const { id: phoneId } = await models.phone.create({
        number: req.body.phone,
        approved: true,
        countryId: req.body.phoneCountryId,
        userId: id,
      }, { transaction });
      await models.user.update({
        phoneId,
      }, { where: { id }, transaction });
    }
    if (req.body.city) {
      await models.address.create({
        ...req.body,
        countryId: req.body.addressCountryId,
        userId: id,
      }, { transaction });
    }
    await transaction.commit();
    const user = await models.user.findByPk(id, { include: [{ all: true }] });
    user.addServices(req.body.service);
    sendNewUserPasswordEmail(password, email).then();
    return res.responseHandler(user);
  } catch (error) {
    if (transaction.finished !== 'commit') {
      await transaction.rollback();
    }
    return next(error);
  }
}

export async function updateUser(req, res, next) {
  const transaction = await models.sequelize.transaction();
  try {
    const user = await models.user.update({
      roleId: req.body.roleId,
    },
    {
      where: { id: req.params.id },
      returning: true,
    });
    return res.responseHandler(user);
  } catch (error) {
    if (transaction.finished !== 'commit') {
      await transaction.rollback();
    }
    return next(error);
  }
}

export function getPermissions(req, res, next) {
  try {
    const arrayOfPermissions = Object.keys(permissions)
      .filter(item => Object.prototype.hasOwnProperty.call(permissions, item))
      .map(item => ({ name: item, value: permissions[item] }));
    return res.responseHandler(arrayOfPermissions);
  } catch (error) {
    return next(error);
  }
}

export async function test(req, res, next) {
  try {
    // const r = await models.product.create({
    //   sku: 'KKF523MsN123',
    //   barcode: '48600s759414',
    //   name: 'New Product Name',
    //   isBlocked: false,
    // eslint-disable-next-line max-len
    //   description: 'Non nulla esse qui reprehenderit culpa nulla cupidatat ut. Proident consequat labore est ullamco laborum ipsum reprehenderit. Eu amet quis ex aliquip magna elit. Do eiusmod cupidatat ut nostrud do velit enim excepteur adipisicing dolore duis.',
    //   keywords: 'New',
    //   price: 170,
    //   minPrice: 125.5,
    //   unit: 1,
    //   step: 1,
    //   createdByUserId: 3,
    //   updatedByUserId: 3,
    //   tags: [
    //     { name: 'Gammammm', createdByUserId: 3, updatedByUserId: 3 },
    //   ],
    // }, {
    //   include: [models.tag],
    // });
    const user = await models.user.findOne();
    console.log(models.user.prototype);
    return res.responseHandler(await user.countSales());
  } catch (error) {
    return next(error);
  }
}

export async function getSetting(req, res, next) {
  try {
    const setting = await models.setting.findByPk(1);
    return setting.value;
  } catch (error) {
    return next(error);
  }
}
