import { Op } from 'sequelize';
import models from '../../models';
import updatedData from '../../utils/sanitizer';
import { BadRequest } from '../errorHandler';
import * as permissions from '../user/permissions'

export async function setStatus(status, userId, orderId) {
  try {
    const response = await models.orderChangelog.create({
      orderId,
      status,
      userId,
    });
    return response;
  } catch (error) {
    throw new BadRequest();
  }
}

export async function showAll(req, res, next) {
  try {
    const options = {
      include: [{ all: true, nested: true }],
    }
    if (!(req.permission & permissions.ORDER_VIEW)) {
      options.where = {
        client_id: req.user.id
      }
    }
    const orders = await models.order.findAll(options);
    return res.responseHandler(orders);
  } catch (error) {
    return next(error);
  }
}

export async function show(req, res, next) {
  try {
    const order = await models.order.findOne({
      where: { id: req.params.id },
      include: [{ all: true, nested: true }],
    });
    return res.responseHandler(order);
  } catch (error) {
    return next(error);
  }
}

export async function test(req, res, next) {
  try {
    const user = await models.user.findByPk(1);
    const response = await user.hasPhone();
    return res.responseHandler(response);
  } catch (error) {
    return next(error);
  }
}

export async function store(req, res, next) {
  try {
    const order = await models.order.create({
      ...req.body,
      orderStatusId: 1,
      client_id: req.user.id,
    });
    order.addServices(req.body.services);
    order.addImages(req.body.images);
    return res.responseHandler(order);
  } catch (error) {
    return next(error);
  }
}

export async function update(req, res, next) {
  try {
    const order = await models.order.update({ ...req.body, ...req.updatedBy }, {
      where: {
        id: req.params.id,
      },
      returning: true,
    });
    return res.responseHandler(updatedData(order));
  } catch (error) {
    return next(error);
  }
}

export async function review(req, res, next) {
  try {
    const order = await models.orderReview.upsert(req.body, {
      where: {
        [Op.and]: [
          { orderId: req.body.orderId },
          { companyId: req.body.companyId },
        ],
      },
      include: [{ all: true, nested: true }],
      returning: true,
    });
    return res.responseHandler(updatedData(order));
  } catch (error) {
    return next(error);
  }
}

export async function updateReview(req, res, next) {
  try {
    await models.orderReview.update({
      accepted: true,
    }, {
      where: { id: req.params.id },
      returning: true,
    });
    await models.order.update({
      orderStatusId: 2,
    }, {
      where: { id: req.body.orderId },
      returning: true,
    });
    return res.responseHandler({
      orderId: req.body.orderId,
    });
  } catch (error) {
    return next(error);
  }
}

export function bulkUpdate(req, res, next) {
  try {
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function remove(req, res, next) {
  try {
    const order = await models.order.destroy({
      where: {
        id: req.params.id,
      },
    });
    return res.responseHandler(order);
  } catch (error) {
    return next(error);
  }
}

export function bulkRemove(req, res, next) {
  try {
    return next();
  } catch (error) {
    return next(error);
  }
}
