import {
  show,
  store,
  showAll,
  update,
  remove,
  bulkUpdate,
  bulkRemove,
  review,
  updateReview
} from './orderController';
import { checkAuthentication, checkPermissions } from '../../utils/auth';
import {
  orderUpdateValidator,
  orderValidator,
} from '../../utils/requestValidations';
import { ORDER_UPDATE } from '../user/permissions';

export default (router) => {
  // ---------------------------------------------------------------------------------------
  router.use(checkAuthentication);
  // ---------------------------------------------------------------------------------------
  router.get('/', showAll);
  router.post('/', store);
  router.post('/review', review);
  router.put('/review/:id', updateReview);
  // ---------------------------------------------------------------------------------------
  router.get('/:id', show);
  // =======================================================================================
  router.use(checkPermissions(ORDER_UPDATE));
  // =======================================================================================
  router.put('/:id', orderUpdateValidator, update);
  // ---------------------------------------------------------------------------------------
  router.put('/update/', bulkUpdate);
  // ---------------------------------------------------------------------------------------
  router.delete('/:id', orderValidator, remove);
  // ---------------------------------------------------------------------------------------
  router.delete('/remove/', bulkRemove);
  // ---------------------------------------------------------------------------------------
};
