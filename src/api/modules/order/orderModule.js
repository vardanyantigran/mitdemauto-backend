import * as express from 'express';
import Routes from './orderRouter';

class OrderModule {
  constructor(apiRouter) {
    this.router = express.Router();
    this.apiRouter = apiRouter;
  }

  createEndpoints() {
    this.assignRouter();
    this.assignEndpoints();
  }

  assignRouter() {
    this.apiRouter.use('/order', this.router);
  }

  assignEndpoints() {
    Routes(this.router);
  }
}
export default apiRouter => new OrderModule(apiRouter);
