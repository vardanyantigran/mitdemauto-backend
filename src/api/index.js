import AuthModule from './modules/auth/authModule';
import UserModule from './modules/user/userModule';
import CategoryModule from './modules/category/categoryModule';
import CountryModule from './modules/country/countryModule';
import ServiceModule from './modules/service/serviceModule';
import CarModule from './modules/car/carModule';
import ImageModule from './modules/image/imageModule';
import RoleModule from './modules/role/roleModule';
import OrderModule from './modules/order/orderModule';
import OptionModule from './modules/option/optionModule';

export default function Router(router) {
  const modules = [];
  const auth = AuthModule(router);
  const user = UserModule(router);
  const category = CategoryModule(router);
  const country = CountryModule(router);
  const service = ServiceModule(router);
  const car = CarModule(router);
  const image = ImageModule(router);
  const role = RoleModule(router);
  const order = OrderModule(router);
  const option = OptionModule(router);

  modules.push(auth);
  modules.push(user);
  modules.push(category);
  modules.push(country);
  modules.push(service);
  modules.push(car);
  modules.push(image);
  modules.push(role);
  modules.push(order);
  modules.push(option);

  modules.forEach((module) => {
    module.createEndpoints();
  });
  return router;
}
