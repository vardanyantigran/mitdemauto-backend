import crypto from 'crypto';
import fs from 'fs';
import path from 'path';
import handlebars from 'handlebars';
import request from 'request';
import { BadRequest } from '../modules/errorHandler';

const algorithm = 'aes-192-cbc';
const key = crypto.scryptSync(process.env.MAIL_SECRET, 'salt', 24);
const iv = Buffer.alloc(16, 0);

function encrypt(text) {
  const cipher = crypto.createCipheriv(algorithm, key, iv);
  let encrypted = cipher.update(text, 'utf8', 'hex');
  encrypted += cipher.final('hex');
  return encrypted;
}

export function decrypt(text) {
  try {
    const decipher = crypto.createDecipheriv(algorithm, key, iv);
    let decrypted = decipher.update(text, 'hex', 'utf8');
    decrypted += decipher.final('utf8');
    return decrypted;
  } catch (error) {
    throw new BadRequest();
  }
}

export function sendSlackMessage(message) {
  return new Promise((resolve, reject) => {
    const options = {
      method: 'POST',
      url: `${process.env.SLACK_URL}/services/TJ46Q7DFZ/BJ2V496E6/${process.env.SLACK_CH_MAIN}`,
      headers:
      {
        'Content-Type': 'application/json',
      },
      body: { text: message },
      json: true,
    };
    request(options, (error, response, body) => {
      if (error) reject(error);
      resolve(body);
    });
  });
}

// eslint-disable-next-line import/prefer-default-export
export async function sendSignUpEmail(code, email, firstName = 'Dear', lastName = 'guest') {
  try {
    const mailingCrypto = encrypt(`${code}$${email.id}`);
    const url = `${process.env.URL}/auth/approveemail/${mailingCrypto}`;
    const html = await fs.readFileSync(path.join(__dirname, 'mailTemplates', 'signUpMailTemplate.html'), { encoding: 'utf-8' });
    const options = {
      hiUserName: `Hi ${firstName} ${lastName}`,
      thankYouText: `Thanks for
      getting started with MitDemAuto! To finish signing up, you just need to confirm that we got your
      email right.`,
      url,
      btnText: 'Confirm Your Email',
      mistakeText: `If you received
      this email by mistake, simply delete it.`,
      facebook: 'https://www.facebook.com/mitdemauto.at/',
      instagram: 'https://instagram.com/mitdemauto.at',
      email: 'info@mitdemauto.at',
      web: 'https://mitdemauto.at',
      apiUrl: process.env.URL,
    };
    console.log(url);
    const template = handlebars.compile(html)(options);
    const mailOptions = {
      from: process.env.MAIL_USERNAME,
      to: email.email,
      subject: 'Confirm your email',
      html: template,
    };
    // if (process.env.NODE_ENV === 'development') {
      // eslint-disable-next-line no-new
    sendSlackMessage(`\`\`\`ACTIVATION CODE: ${mailingCrypto}\`\`\``);
    console.log(mailingCrypto);
    // }

    return await new Promise((resolve, reject) => {
      global.transportMenu4u.sendMail(mailOptions, (error, response) => {
        if (error) reject(error);
        resolve(response);
      });
    });
  } catch (error) {
    throw error;
  }
}

// eslint-disable-next-line import/prefer-default-export
export async function sendForgotPasswordEmail(code, email, firstName = 'Dear', lastName = 'guest') {
  try {
    const mailingCrypto = encrypt(`${code}$${email.id}`);
    const url = `${process.env.URL}/auth/password/${mailingCrypto}`;
    const html = await fs.readFileSync(path.join(__dirname, 'mailTemplates', 'signUpMailTemplate.html'), { encoding: 'utf-8' });
    const options = {
      hiUserName: `Hi ${firstName} ${lastName}`,
      thankYouText: `Thanks for
      getting started with MitDemAuto! To finish signing up, you just need to confirm that we got your
      email right.`,
      url,
      btnText: 'Confirm Your Email',
      mistakeText: `If you received
      this email by mistake, simply delete it.`,
      facebook: 'https://www.facebook.com/mitdemauto.at/',
      instagram: 'https://instagram.com/mitdemauto.at',
      email: 'info@mitdemauto.at',
      web: 'https://mitdemauto.at',
      apiUrl: process.env.URL,
    };
    console.log(url);
    const template = handlebars.compile(html)(options);
    const mailOptions = {
      from: process.env.MAIL_USERNAME,
      to: email.email,
      subject: 'Confirm your email',
      html: template,
    };
    // if (process.env.NODE_ENV === 'development') {
      // eslint-disable-next-line no-new
    sendSlackMessage(`\`\`\`ACTIVATION CODE: ${mailingCrypto}\`\`\``);
    console.log(mailingCrypto);
    // }

    return await new Promise((resolve, reject) => {
      global.transportMenu4u.sendMail(mailOptions, (error, response) => {
        if (error) reject(error);
        resolve(response);
      });
    });
  } catch (error) {
    throw error;
  }
}

export async function sendNewUserPasswordEmail(password, email, firstName = 'Dear', lastName = 'guest') {
  try {
    const html = await fs.readFileSync(path.join(__dirname, 'mailTemplates', 'signUpMailTemplate.html'), { encoding: 'utf-8' });
    const template = handlebars.compile(html)({ firstName, lastName, password });
    const mailOptions = {
      from: process.env.MAIL_USERNAME,
      to: email.email,
      subject: 'SignUp',
      html: template,
    };
    // if (process.env.NODE_ENV === 'development') {
      // eslint-disable-next-line no-new
    sendSlackMessage(`\`\`\`NEW PASSWORD: ${password}\`\`\``);
    console.log(password);
    // }

    return await new Promise((resolve, reject) => {
      global.transportMenu4u.sendMail(mailOptions, (error, response) => {
        if (error) reject(error);
        resolve(response);
      });
    });
  } catch (error) {
    throw error;
  }
}
