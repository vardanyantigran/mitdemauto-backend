import { Op } from 'sequelize';
import crypto from 'crypto';
import models from '../../models';

// eslint-disable-next-line import/prefer-default-export
export async function isEmailAvailable(email) {
  const temp = await models.email.findOne({
    where: { email },
    attributes: ['id'],
  });
  return !temp;
}

export async function isUsernameAvailable(username) {
  return !await models.user.findOne({
    where: { username },
    attributes: ['id'],
  });
}

export function isEmailValid(email) {
  return !!isEmailAvailable(email);
}

export function isUsernameValid(username) {
  return !!isUsernameAvailable(username);
}

export function isSignInValid(username) {
  // eslint-disable-next-line no-bitwise
  if (!~username.indexOf('@') && username.match(/^(?=.{6,64}$)(?![_.-])(?!.*[_.-]{2})[a-zA-Z0-9._-]+(?<![_.-])$/)) {
    return isUsernameValid(username);
  }
  return isEmailValid(username);
}

export async function isPasswordCorrect(password, { req: { user: { id = -1 } = {} } = {} }) {
  const user = await models.user.findOne({
    where: { id },
    attributes: ['password'],
  });
  return user.password === crypto.createHash('sha1').update(password).digest('hex');
}

export async function isCountryValid(id) {
  return !!await models.country.findByPk(id, {
    attributes: ['id'],
  });
}

export async function isCategoryValid(id) {
  return !!await models.category.findByPk(id, {
    attributes: ['id'],
  });
}

export async function isServiceValid(id) {
  return !!await models.service.findByPk(id, {
    attributes: ['id'],
  });
}

export async function isMeasurementValid(id) {
  return !!await models.measurement.findByPk(id, {
    attributes: ['id'],
  });
}

export async function isCarValid(id) {
  return !!await models.car.findByPk(id, {
    attributes: ['id'],
  });
}

export async function isRoleValid(id) {
  return !!await models.role.findByPk(id, {
    attributes: ['id'],
  });
}

export async function isTagNameAvailable(name) {
  return !await models.tag.findOne({
    where: { name },
    attributes: ['id'],
  });
}

export async function isSkuAvailable(sku) {
  return !await models.product.findOne({
    where: { sku },
    attributes: ['id'],
  });
}

export async function isBarcodeAvailable(barcode) {
  return !await models.product.findOne({
    where: { barcode },
    attributes: ['id'],
  });
}

export async function isAttributeValid(id) {
  return !!await models.attribute.findByPk(id, {
    attributes: ['id'],
  });
}

export async function isAttributeArrayValid(array) {
  const attributes = await models.attribute.findAll({
    where: {
      id: { [Op.or]: array.map(attribute => attribute.id) },
    },
    attributes: ['id'],
  });
  return attributes.length === array.length;
}

export async function isProductValid(id) {
  return !!await models.product.findByPk(id, {
    attributes: ['id'],
  });
}

export async function isVariantValid(id) {
  return !!await models.variant.findByPk(id, {
    attributes: ['id'],
  });
}

export async function isVariantArrayValid(array) {
  const variants = await models.variant.findAll({
    where: {
      id: { [Op.or]: array },
    },
    attributes: ['id'],
  });
  return variants.length === array.length;
}

export async function isOrderValid(id) {
  return !!await models.order.findByPk(id, {
    attributes: ['id'],
  });
}

export async function isImageValid(id) {
  return !!await models.image.findByPk(id, {
    attributes: ['id'],
  });
}

export async function isImageArrayValid(array) {
  const images = await models.image.findAll({
    where: {
      id: { [Op.or]: array },
    },
    attributes: ['id'],
  });
  return images.length === array.length;
}

export async function isAddressValid(id) {
  return !!await models.address.findByPk(id, {
    attributes: ['id'],
  });
}

export async function isUserValid(id) {
  return !!await models.user.findByPk(id, {
    attributes: ['id'],
  });
}

export async function isPhoneValid(id) {
  return !!await models.phone.findByPk(id, {
    attributes: ['id'],
  });
}

export async function isUserValidByFirebase(firebaseId) {
  return !!await models.user.findOne({
    where: {
      [Op.and]: [
        { firebaseId },
        {
          [Op.not]: { password: null },
        },
      ],
    },
  }, {
    attributes: ['id'],
  });
}
