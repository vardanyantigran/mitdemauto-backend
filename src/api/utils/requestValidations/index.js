import {
  body, check, validationResult, query,
} from 'express-validator/check';
import {
  isEmailAvailable,
  isUsernameAvailable,
  isCountryValid,
  isCategoryValid,
  isServiceValid,
  isMeasurementValid,
  isCarValid,
  isRoleValid,
  isSkuAvailable,
  isBarcodeAvailable,
  isAttributeValid,
  isProductValid,
  isVariantValid,
  isOrderValid,
  isImageValid,
  isImageArrayValid,
  isVariantArrayValid,
  isAttributeArrayValid,
  isUserValid,
  isAddressValid,
  isPhoneValid,
} from './customValidations';

const simpleText = value => body(value).optional().isLength({ max: 255 }).withMessage('max length 255 chars');
const requiredSimpleText = value => body(value)
  .not().isEmpty().withMessage(`${value} is required`)
  .isLength({ min: 1, max: 255 })
  .withMessage('max length 255 chars');
const notEmpty = value => check(value).not().isEmpty().withMessage(`${value} is required`);
const email = () => check('email').isEmail().withMessage('must be valid email address');
const username = () => check('username').isEmail().withMessage('must be valid email address');
const password = () => body('password').isLength({ min: 6, max: 255 }).withMessage('min 6, max 255');
const dateInt = value => body(value).toInt().isInt().withMessage('must be an integer');
const integerValue = value => body(value).toInt().isInt()
  .optional()
  .withMessage('must be an integer');
const doubleValue = value => body(value).toFloat().isFloat()
  .optional()
  .withMessage('must be an integer');
const countryId = value => check(value).toInt().isInt()
  .custom(isCountryValid)
  .withMessage(`country with id:${value} is undefined`);
const categoryId = value => check(value).toInt().isInt()
  .custom(isCategoryValid)
  .withMessage(`category with id:${value} is undefined`);
const serviceId = value => check(value).toInt().isInt()
  .custom(isServiceValid)
  .withMessage(`service with id:${value} is undefined`);
const measurementId = value => check(value).toInt().isInt()
  .custom(isMeasurementValid)
  .withMessage(`measurement with id:${value} is undefined`);
const carId = value => check(value).toInt().isInt()
  .custom(isCarValid)
  .withMessage(`tag with id:${value} is undefined`);
const roleId = value => check(value).toInt().isInt()
  .custom(isRoleValid)
  .withMessage(`role with id:${value} is undefined`);
const attributeId = value => check(value).toInt().isInt()
  .custom(isAttributeValid)
  .withMessage(`attribute with id:${value} is undefined`);
const attributeIdArray = value => check(value).isArray().optional()
  .custom(isAttributeArrayValid)
  .withMessage(`attributes with id:${value} is undefined`);
const productId = value => check(value).toInt().isInt()
  .custom(isProductValid)
  .withMessage(`product with id:${value} is undefined`);
const variantId = value => check(value).toInt().isInt()
  .custom(isVariantValid)
  .withMessage(`variant with id:${value} is undefined`);
const variantIdArray = value => check(value).isArray().optional()
  .custom(isVariantArrayValid)
  .withMessage(`variants with id:${value} is undefined`);
const orderId = value => check(value).toInt().isInt()
  .custom(isOrderValid)
  .withMessage(`order with id:${value} is undefined`);
const imageId = value => check(value).toInt().isInt()
  .custom(isImageValid)
  .withMessage(`image with id:${value} is undefined`);
const userId = value => check(value).toInt().isInt()
  .custom(isUserValid)
  .withMessage(`user with id:${value} is undefined`);
const addressId = value => check(value).toInt().isInt()
  .custom(isAddressValid)
  .withMessage(`address with id:${value} is undefined`);
const phoneId = value => check(value).toInt().isInt()
  .custom(isPhoneValid)
  .withMessage(`phone with id:${value} is undefined`);
const imageIdArray = value => check(value).isArray().optional()
  .custom(isImageArrayValid)
  .withMessage(`images with id:${value} is undefined`);
// const firebaseId = () => check('firebaseId').isArray().optional()
//   .custom(isUserValidByFirebase)
//   .withMessage(`user with firebaseId:${'firebaseId'} is undefined`);
const phone = () => body('phone').isMobilePhone().withMessage('wrong phone number');
const zip = () => body('zip').isPostalCode('any').withMessage('wrong postal code');
const isBlocked = () => body('isBlocked').toBoolean().withMessage('must be boolean');
const keywords = () => body('keywords')
  .optional()
  .trim()
  .isLength({ min: 0, max: 255 })
  .withMessage('max length 255 chars');
const image = () => body('image').optional().isLength({ min: 0, max: 255 }).withMessage('max length 255 chars');
const sku = () => body('sku').optional().isLength({ min: 0, max: 255 }).withMessage('max length 255 chars');
const barcode = () => body('barcode').optional().isLength({ min: 0, max: 255 }).withMessage('max length 255 chars');

export async function signUpValidator(req, res, next) {
  try {
    await Promise.all([
      // email().custom(isEmailAvailable).withMessage('email already in use'),
      username().custom(isUsernameAvailable).withMessage('email already in use'),
      password(),
      requiredSimpleText('firstName'),
      requiredSimpleText('lastName'),
      dateInt('birthday').optional(),
      countryId('phoneCountryId').optional(),
      requiredSimpleText('phone'),
      countryId('addressCountryId').optional(),
      zip().optional(),
      simpleText('region'),
      simpleText('city'),
      simpleText('line1'),
      simpleText('line2'),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function userCreateValidator(req, res, next) {
  try {
    await Promise.all([
      // email().custom(isEmailAvailable).withMessage('email already in use'),
      // username().custom(isUsernameAvailable).withMessage('username already in use'),
      roleId('roleId'),
      requiredSimpleText('firstName'),
      requiredSimpleText('lastName'),
      dateInt('birthday').optional(),
      countryId('phoneCountryId').optional(),
      phone().optional(),
      countryId('addressCountryId').optional(),
      zip().optional(),
      simpleText('region'),
      simpleText('city'),
      simpleText('line1'),
      simpleText('line2'),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function userUpdateValidator(req, res, next) {
  try {
    await Promise.all([
      roleId('roleId'),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function signInValidator(req, res, next) {
  try {
    await Promise.all([
      // oneOf([
      //   username().custom(isUsernameValid).withMessage('the username is undefined'),
      //   email(),
      // ]),
      // check('username').custom(isSignInValid).withMessage('username or email is incorrect'),
      body('password'),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function checkEmailValidator(req, res, next) {
  try {
    await Promise.all([
      email().custom(isEmailAvailable).withMessage('email already in use'),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function checkUsernameValidator(req, res, next) {
  try {
    await Promise.all([
      username().custom(isUsernameAvailable).withMessage('username already in use'),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function activateEmailValidator(req, res, next) {
  try {
    await Promise.all([
      notEmpty('activateCode'),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function roleValidator(req, res, next) {
  try {
    await Promise.all([
      requiredSimpleText('name'),
      integerValue('value'),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function roleUpdateValidator(req, res, next) {
  try {
    await Promise.all([
      roleId('id'),
      simpleText('name'),
      integerValue('value'),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function categoryValidator(req, res, next) {
  try {
    await Promise.all([
      categoryId('id'),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function storeCategoryValidator(req, res, next) {
  try {
    await Promise.all([
      requiredSimpleText('name'),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function updateCategoryValidator(req, res, next) {
  try {
    await Promise.all([
      categoryId('id'),
      simpleText('name'),
      isBlocked().optional(),
      keywords(),
      image(),
      categoryId('categoryId').optional(),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function countryValidator(req, res, next) {
  try {
    await Promise.all([
      countryId('id'),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function serviceValidator(req, res, next) {
  try {
    await Promise.all([
      serviceId('id'),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function storeServiceValidator(req, res, next) {
  try {
    await Promise.all([
      requiredSimpleText('name'),
      body('acceptImage').isBoolean(),
      categoryId('categoryId').optional(),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function updateServiceValidator(req, res, next) {
  try {
    await Promise.all([
      serviceId('id'),
      simpleText('name'),
      body('acceptImage').isBoolean().optional(),
      categoryId('categoryId').optional(),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function measurementValidator(req, res, next) {
  try {
    await Promise.all([
      measurementId('id'),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function measurementCreateValidator(req, res, next) {
  try {
    await Promise.all([
      requiredSimpleText('name'),
      simpleText('shortName'),
      doubleValue('unit'),
      doubleValue('step'),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function measurementUpdateValidator(req, res, next) {
  try {
    await Promise.all([
      measurementId('id'),
      simpleText('name'),
      simpleText('shortName'),
      doubleValue('unit'),
      doubleValue('step'),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function carValidator(req, res, next) {
  try {
    await Promise.all([
      carId('id'),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function storeCarValidator(req, res, next) {
  try {
    await Promise.all([
      requiredSimpleText('name'),
      simpleText('mark'),
      simpleText('odometer'),
      simpleText('model'),
      requiredSimpleText('vin'),
      userId('userId'),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function updateCarValidator(req, res, next) {
  try {
    await Promise.all([
      carId('id'),
      simpleText('name'),
      simpleText('mark'),
      simpleText('odometer'),
      simpleText('model'),
      simpleText('vin'),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function attributeValidator(req, res, next) {
  try {
    await Promise.all([
      attributeId('id'),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function storeAttributeValidator(req, res, next) {
  try {
    await Promise.all([
      requiredSimpleText('name'),
      simpleText('defaultValue'),
      simpleText('options'),
      measurementId('measurementId'),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function updateAttributeValidator(req, res, next) {
  try {
    await Promise.all([
      attributeId('id'),
      simpleText('name'),
      simpleText('defaultValue'),
      simpleText('options'),
      measurementId('measurementId'),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function getProductValidator(req, res, next) {
  try {
    await Promise.all([
      productId('id'),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function getProductOptionValidator(req, res, next) {
  try {
    await Promise.all([
      query('options').not().isEmpty(),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function storeProductValidator(req, res, next) {
  try {
    await Promise.all([
      requiredSimpleText('name'),
      sku().custom(isSkuAvailable).withMessage(`sku name '${req.body.sku}' alraedy exist`),
      barcode().custom(isBarcodeAvailable).withMessage(`barcode name '${req.body.barcode}' alraedy exist`),
      body('description').optional().isLength({ max: 2047 }).withMessage('max length 2047 chars'),
      keywords(),
      doubleValue('price'),
      doubleValue('min_price'),
      doubleValue('unit'),
      doubleValue('step'),
      categoryId('categoryId'),
      countryId('countryId'),
      measurementId('measurementId'),
      imageId('defaultImageId'),
      imageIdArray('images'),
      variantIdArray('variants'),
      attributeIdArray('attributes'),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function updateProductValidator(req, res, next) {
  try {
    await Promise.all([
      productId('id'),
      simpleText('name'),
      sku().custom(isSkuAvailable).withMessage(`sku name '${req.body.sku}' alraedy exist`),
      barcode().custom(isBarcodeAvailable).withMessage(`barcode name '${req.body.barcode}' alraedy exist`),
      simpleText('description'),
      keywords(),
      doubleValue('price'),
      doubleValue('min_price'),
      doubleValue('unit'),
      doubleValue('step'),
      categoryId('categoryId'),
      countryId('countryId'),
      measurementId('measurementId'),
      imageId('defaultImageId'),
      imageIdArray('images'),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function variantValidator(req, res, next) {
  try {
    await Promise.all([
      variantId('id'),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function variantCreateValidator(req, res, next) {
  try {
    await Promise.all([
      requiredSimpleText('name'),
      body('options').isArray().optional().withMessage('options must be an array of string'),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function variantUpdateValidator(req, res, next) {
  try {
    await Promise.all([
      variantId('id'),
      simpleText('name'),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}


export async function orderValidator(req, res, next) {
  try {
    await Promise.all([
      orderId('id'),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function orderCreateValidator(req, res, next) { // TO DO
  try {
    await Promise.all([
      body('options').isArray().optional().withMessage('options must be an array of string'),
      countryId('addressCountryId').optional(),
      zip().optional(),
      simpleText('region'),
      simpleText('city'),
      simpleText('line1'),
      simpleText('line2'),
      email().custom(isEmailAvailable).optional(),
      simpleText('phone'),
      check('saveAddress').isBoolean().optional(),
      requiredSimpleText('firebaseId'),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function orderCreateAsUserValidator(req, res, next) { // TO DO
  try {
    const validators = req.isSignIn
      ? [
        userId('userId'),
        addressId('addressId').optional(),
        phoneId('phoneId').optional(),
      ]
      : [
        requiredSimpleText('phone'),
      ];
    await Promise.all(validators.map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function orderUpdateValidator(req, res, next) { // TO DO
  try {
    await Promise.all([
      orderId('id'),
      simpleText('name'),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}
