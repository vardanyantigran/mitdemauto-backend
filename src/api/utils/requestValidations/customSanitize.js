import models from '../../models';

export async function categorySanitizer(id) {
  const category = await models.category.findByPk(id);
  return category;
}

export function emptyString(string) {
  return string === '' ? null : string;
}
