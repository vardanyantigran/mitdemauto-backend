import multer from 'multer';
import uuid from 'uuid/v4';

const fileName = uuid();
// const productStorage = multer.diskStorage({
//   destination(req, file, cb) {
//     cb(null, 'uploads/image/product/');
//   },
//   filename(req, file, cb) {
//     cb(null, fileName);
//   },
// });

const imageStorage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, 'uploads/image/');
  },
  filename(req, file, cb) {
    cb(null, fileName);
  },
});

function sanitizeFile(file, cb) {
  // Define the allowed extension
  const fileExts = ['png', 'jpg', 'jpeg', 'gif'];
  // Check allowed extensions
  const isAllowedExt = fileExts.includes(file.originalname.split('.')[1].toLowerCase());
  // Mime type must be an image
  const isAllowedMimeType = file.mimetype.startsWith('image/');
  if (isAllowedExt && isAllowedMimeType) {
    return cb(null, true); // no errors
  }

  // pass error msg to callback, which can be displaye in frontend
  return cb('Error: File type not allowed!');
}

export const productImageUpload = multer({
  dest: 'uploads/image/product/',
  limits: {
    fileSize: 1024,
  },
  fileFilter: (req, file, cb) => {
    sanitizeFile(file, cb);
  },
}).single('img');

export const imageUpload = multer({
  storage: imageStorage,
});
