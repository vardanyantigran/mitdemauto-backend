import jwt from 'jsonwebtoken';
import { AuthError, AccessDenied } from '../modules/errorHandler';

function redisGet(key) {
  return new Promise((resolve, reject) => {
    global.redisClient.get(key, (error, response) => {
      if (error) reject(error);
      resolve(response);
    });
  });
}

export function redisDel(key) {
  return new Promise((resolve, reject) => {
    global.redisClient.del(key, (error, response) => {
      if (error) reject(error);
      resolve(response);
    });
  });
}

// eslint-disable-next-line no-unused-vars
export function rediskeys(key) {
  return new Promise((resolve, reject) => {
    global.redisClient.keys(key, (error, response) => {
      if (error) reject(error);
      resolve(response);
    });
  });
}
// eslint-disable-next-line no-unused-vars
export function redismGet(key) {
  return new Promise((resolve, reject) => {
    global.redisClient.mget(key, (error, response) => {
      if (error) reject(error);
      resolve(response);
    });
  });
}

export async function checkAuthentication(req, res, next) {
  try {
    let currentSessionId;
    try {
      const { sessionId } = jwt.verify(req.headers.authorization, process.env.JWT_SECRET);
      currentSessionId = sessionId;
      console.log('\x1b[33m%s\x1b[0m', '*******************************************************************************************************************');
      console.log('\x1b[33m%s\x1b[0m', `*********************************\x1b[37m SESSION ID: ${sessionId}\x1b[33m ********************************`);
      console.log('\x1b[33m%s\x1b[0m', '*******************************************************************************************************************');
    } catch (error) {
      throw new AuthError();
    }
    const user = JSON.parse(await redisGet(currentSessionId));
    if (!user) {
      throw new AuthError();
    }
    req.isSignIn = true;
    req.user = user;
    req.sessionId = currentSessionId;
    req.permission = user.role.value;
    return next();
  } catch (error) {
    req.permission = null;
    req.sessionId = null;
    req.user = null;
    req.isSignIn = false;
    return next(error);
  }
}

export async function checkIsSignIn(req, res, next) {
  try {
    let currentSessionId;
    try {
      const { sessionId } = jwt.verify(req.headers.authorization, process.env.JWT_SECRET);
      currentSessionId = sessionId;
      console.log('\x1b[33m%s\x1b[0m', '*******************************************************************************************************************');
      console.log('\x1b[33m%s\x1b[0m', `*********************************\x1b[37m SESSION ID: ${sessionId}\x1b[33m ********************************`);
      console.log('\x1b[33m%s\x1b[0m', '*******************************************************************************************************************');
    } catch (error) {
      req.isSignIn = false;
      return next();
    }
    const user = JSON.parse(await redisGet(currentSessionId));
    if (!user) {
      req.isSignIn = false;
      return next();
    }
    req.isSignIn = true;
    req.user = user;
    req.sessionId = currentSessionId;
    req.permission = user.role.value;
    req.updatedBy = { updatedByUserId: user.id };
    req.createdBy = { updatedByUserId: user.id, createdByUserId: user.id };
    return next();
  } catch (error) {
    req.permission = null;
    req.sessionId = null;
    req.user = null;
    req.isSignIn = false;
    return next(error);
  }
}

export function checkPermissions(permission) {
  return (req, res, next) => {
    try {
      // eslint-disable-next-line no-bitwise
      if ((permission & req.permission) === 0) {
        throw new AccessDenied();
      }
      return next();
    } catch (error) {
      return next(error);
    }
  };
}
