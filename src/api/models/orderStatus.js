module.exports = (sequelize, DataTypes) => {
  const orderStatus = sequelize.define('orderStatus', {
    status: {
      type: DataTypes.STRING,
      unique: true,
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
  }, {
    tableName: 'order_status',
    underscored: true,
  });
  orderStatus.associate = function associate(models) {
    models.orderStatus.hasMany(models.order);
  };
  return orderStatus;
};
