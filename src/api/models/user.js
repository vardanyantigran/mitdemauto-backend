module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define('user', {
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    username: {
      type: DataTypes.STRING,
      unique: true,
    },
    password: {
      type: DataTypes.STRING,
    },
    birthday: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: null,
    },
    isBlocked: {
      type: DataTypes.BOOLEAN,
    },
    companyName: {
      type: DataTypes.STRING,
    },
    tin: {
      type: DataTypes.STRING,
    },
    facebookId: {
      type: DataTypes.STRING,
      unique: true,
    },
    firebaseId: {
      type: DataTypes.STRING,
      unique: true,
    },
    googleId: {
      type: DataTypes.STRING,
      unique: true,
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
  }, {
    tableName: 'users',
    underscored: true,
    defaultScope: {
      attributes: { exclude: ['password', 'facebookId', 'googleId'] },
    },
  });
  user.associate = function associate(models) {
    models.user.belongsTo(models.role);
    models.user.hasOne(models.address);
    models.user.hasOne(models.phone);
    models.user.hasOne(models.email);
    models.user.hasOne(models.image);
    models.user.hasMany(models.car);
    models.user.belongsToMany(models.service, {
      through: 'user_services',
      foreignKey: 'user_id',
      constraints: false,
    });
    models.user.hasMany(models.order, {
      as: 'client',
      foreignKey: 'client_id',
    });
    models.user.hasMany(models.order, {
      as: 'company',
      foreignKey: 'company_id',
    });
    models.user.hasMany(models.orderReview, {
      foreignKey: 'companyId',
    });
  };
  return user;
};
