module.exports = (sequelize, DataTypes) => {
  const service = sequelize.define('service', {
    name: {
      type: DataTypes.STRING,
    },
    description: {
      type: DataTypes.TEXT,
    },
    acceptImage: {
      type: DataTypes.BOOLEAN,
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
  }, {
    tableName: 'services',
    underscored: true,
  });
  service.associate = function associate(models) {
    models.service.belongsToMany(models.user, {
      through: 'user_services',
      foreignKey: 'service_id',
      constraints: false,
    });
    models.service.belongsToMany(models.order, {
      through: 'order_services',
      foreignKey: 'service_id',
      constraints: false,
    });
    models.service.hasOne(models.image);
  };
  return service;
};
