module.exports = (sequelize, DataTypes) => {
  const orderChangelog = sequelize.define('orderChangelog', {
    date: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    status: {
      type: DataTypes.STRING,
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
  }, {
    tableName: 'order_changelog',
    underscored: true,
    defaultScope: {
      attributes: { exclude: ['updatedAt', 'createdAt'] },
    },
  });
  orderChangelog.associate = function associate(models) {
    models.orderChangelog.belongsTo(models.user);
  };


  return orderChangelog;
};
