module.exports = (sequelize, DataTypes) => {
  const country = sequelize.define('country', {
    name: {
      type: DataTypes.STRING,
    },
    alpha2: {
      type: DataTypes.STRING,
    },
    alpha3: {
      type: DataTypes.STRING,
    },
    region: {
      type: DataTypes.STRING,
    },
    subRegion: {
      type: DataTypes.STRING,
    },
    dialCode: {
      type: DataTypes.STRING,
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
  }, {
    tableName: 'countries',
    underscored: true,
    defaultScope: {
      attributes: { exclude: ['updatedAt', 'createdAt'] },
    },
  });
  return country;
};
