module.exports = (sequelize, DataTypes) => {
  const setting = sequelize.define('setting', {
    key: {
      type: DataTypes.STRING,
    },
    value: {
      type: DataTypes.JSONB,
    },
  }, {
    tableName: 'settings',
    underscored: true,
  });
  return setting;
};
