module.exports = (sequelize, DataTypes) => {
  const phone = sequelize.define('phone', {
    number: {
      type: DataTypes.STRING,
      unique: 'phone_id',
    },
    approved: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    sendedSmsCount: {
      type: DataTypes.INTEGER,
    },
    userId: {
      type: DataTypes.INTEGER,
      unique: 'phone_id',
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
  }, {
    tableName: 'phones',
    underscored: true,
    defaultScope: {
      attributes: { exclude: ['updatedAt', 'createdAt', 'userId', 'countryId'] },
    },
  });
  phone.associate = function associate(models) {
    models.phone.belongsTo(models.country);
  };
  return phone;
};
