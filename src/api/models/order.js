module.exports = (sequelize, DataTypes) => {
  const order = sequelize.define('order', {
    comment: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
  }, {
    tableName: 'orders',
    underscored: true,
    defaultScope: {
      attributes: { exclude: ['updatedAt', 'createdAt'] },
    },
    hooks: {
      afterUpdate: (ord) => {
        ord.createOrderChangelog({
          date: Date.now(),
          orderId: ord.id,
          userId: ord.updatedByUserId,
          status: ord.orderStatusId,
        });
      },
    },
  });
  order.associate = function associate(models) {
    models.order.belongsToMany(models.service, {
      through: 'order_services',
      foreignKey: 'order_id',
      constraints: false,
    });
    models.order.hasMany(models.orderChangelog);
    models.order.belongsTo(models.orderStatus);
    models.order.belongsTo(models.car);
    // models.order.belongsTo({
    //   model: models.user,
    //   as: 'client',
    //   foreignKey: 'client_id',
    // });
    models.order.hasMany(models.image);
    models.order.hasMany(models.orderReview);
  };
  return order;
};
