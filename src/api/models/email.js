module.exports = (sequelize, DataTypes) => {
  const email = sequelize.define('email', {
    email: {
      type: DataTypes.STRING,
      unique: true,
    },
    code: {
      type: DataTypes.STRING,
    },
    approved: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
  }, {
    defaultScope: {
      attributes: { exclude: ['updatedAt', 'createdAt', 'userId', 'code'] },
    },
    scopes: {
      approved: {
        where: { approved: true },
      },
    },
    tableName: 'emails',
    underscored: true,
  });
  email.associate = function associate(models) {
    models.email.belongsTo(models.user);
  };
  return email;
};
