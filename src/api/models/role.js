module.exports = (sequelize, DataTypes) => {
  const role = sequelize.define('role', {
    name: DataTypes.STRING,
    value: DataTypes.BIGINT,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
  }, {
    tableName: 'roles',
    underscored: true,
    defaultScope: {
      attributes: { exclude: ['updatedAt', 'createdAt'] },
    },
  });
  return role;
};
