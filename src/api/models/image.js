module.exports = (sequelize, DataTypes) => {
  const image = sequelize.define('image', {
    url: {
      type: DataTypes.STRING,
    },
    alt: {
      type: DataTypes.STRING,
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
  }, {
    tableName: 'images',
    underscored: true,
    defaultScope: {
      attributes: { exclude: ['updatedAt', 'createdAt'] },
    },
  });
  return image;
};
