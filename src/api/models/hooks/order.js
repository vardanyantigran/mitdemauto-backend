import models from '..';

models.order.afterUpdate((order) => {
  models.orderChangelog.create({
    date: Date.now(),
    orderId: order.id,
    userId: order.updatedByUserId,
    status: order.orderStatusId,
  });
});
