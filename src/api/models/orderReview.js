module.exports = (sequelize, DataTypes) => {
  const orderReview = sequelize.define('orderReview', {
    price: {
      type: DataTypes.DOUBLE,
    },
    start: {
      type: DataTypes.INTEGER,
    },
    end: {
      type: DataTypes.INTEGER,
    },
    accepted: {
      type: DataTypes.BOOLEAN,
    },
    conversation: {
      type: DataTypes.ARRAY(DataTypes.JSONB),
    },
  }, {
    tableName: 'order_review',
    underscored: true,
  });
  orderReview.associate = function associate(models) {
    models.orderReview.belongsTo(models.order);
  };
  return orderReview;
};
