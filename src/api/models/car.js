module.exports = (sequelize, DataTypes) => {
  const car = sequelize.define('car', {
    name: {
      type: DataTypes.STRING,
    },
    mark: {
      type: DataTypes.STRING,
    },
    model: {
      type: DataTypes.STRING,
    },
    odometer: {
      type: DataTypes.STRING,
    },
    vin: {
      type: DataTypes.STRING,
    },
    image: {
      type: DataTypes.STRING,
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
  }, {
    tableName: 'cars',
    underscored: true,
  });
  car.associate = function associate(models) {
    models.car.belongsTo(models.user);
    models.car.hasMany(models.image);
  };
  return car;
};
