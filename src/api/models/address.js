module.exports = (sequelize, DataTypes) => {
  const address = sequelize.define('address', {
    zip: {
      type: DataTypes.STRING,
      unique: 'address_id',
    },
    region: {
      type: DataTypes.STRING,
      unique: 'address_id',
    },
    city: {
      type: DataTypes.STRING,
      unique: 'address_id',
    },
    line1: {
      type: DataTypes.STRING,
      unique: 'address_id',
    },
    line2: {
      type: DataTypes.STRING,
      unique: 'address_id',
    },
    userId: {
      type: DataTypes.INTEGER,
      unique: 'address_id',
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.fn('now'),
    },
  }, {
    tableName: 'addresses',
    underscored: true,
    defaultScope: {
      attributes: { exclude: ['updatedAt', 'createdAt', 'userId', 'countryId'] },
    },
  });
  address.associate = function associate(models) {
    models.address.belongsTo(models.country);
  };
  return address;
};
