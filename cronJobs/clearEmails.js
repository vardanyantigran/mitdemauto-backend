'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    const Op = Sequelize.Op;
    return queryInterface.bulkDelete('emails', {
        created_at: {
          [Op.lt]: new Date(new Date() - 24 * 60 * 60 * 1000)
        },
        approved: false
      }
    , {
      returning: true,
    });
  },

  down: (queryInterface, Sequelize) => {
    return 0;
  }
};
